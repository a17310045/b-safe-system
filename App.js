import 'react-native-gesture-handler';
import React from 'react';

// Importas reactNavigation
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

// Importar pantallas
import contactsScreen from './screens/contactsScreen'
import emergencyScreen from './screens/emergencyScreen'
import forwardScreen from './screens/forwardScreen'
import loadScreen from './screens/loadScreen'
import loginScreen from './screens/loginScreen'
import mainScreen from './screens/mainScreen'
import profileScreen from './screens/profileScreen'
import registryScreen from './screens/registryScreen'
import settingsScreen from './screens/settingsScreen'
import testScreen from './screens/testScreen'

const Stack = createStackNavigator()

const App = () => {


  return (
    <>
      <NavigationContainer>
        
        <Stack.Navigator 
        initialRouteName='loadScreen'
        screenOptions = {{
          headerShown: false
        }}>
          <Stack.Screen
            name = 'contactsScreen'
            component = {contactsScreen}
          />
          <Stack.Screen
            name = 'emergencyScreen'
            component = {emergencyScreen}
          />
          <Stack.Screen
            name = 'forwardScreen'
            component = {forwardScreen}
          />
          <Stack.Screen
            name = 'loadScreen'
            component = {loadScreen}
          />
          <Stack.Screen
            name = 'loginScreen'
            component = {loginScreen}
          />
          <Stack.Screen
            name = 'mainScreen'
            component = {mainScreen}
          />
          <Stack.Screen
            name = 'profileScreen'
            component = {profileScreen}
          />
          <Stack.Screen
            name = 'registryScreen'
            component = {registryScreen}
          />
          <Stack.Screen
            name = 'settingsScreen'
            component = {settingsScreen}
          />
          <Stack.Screen
            name = 'testScreen'
            component = {testScreen}
          />
        </Stack.Navigator>

      </NavigationContainer>
    </>
  );
};


export default App;
