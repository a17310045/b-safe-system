import React from 'react';
import { Image, StyleSheet, View, Text, TouchableHighlight, ScrollView, Linking } from 'react-native';
import { Card, CardItem, Body, Content } from "native-base";

const testScreen = ({navigation}) => {
    return (
            <ScrollView contentContainerStyle={{flex: 1, backgroundColor:'#0a303f'}}>
                <View>
                    <Image style={{height: 50, width: 50}} source={require('../assets/icons/menu.png')}></Image>
                </View>
    {/* ---------------------------------1--------------------------------------- */}
                <View style={{flex: 1,justifyContent:'center',paddingHorizontal:25}}>
                    <Text style={{color:'white',fontSize:30,textAlign:'center'}}>!*Usuario* ha activado el modo de pánico!</Text>
                </View>
    {/* ----------------------------------2-------------------------------------- */}
                <View style={{flex: 1,paddingVertical:10}}>
                    <Content style={{paddingHorizontal:20}}>
                        <Card>
                            <CardItem style={{backgroundColor:'#00b400'}}>
                                <Body style={{paddingHorizontal:50}}>
                                    <Text style={{color:'white', fontSize:15,textAlign:'center'}}>
                                        El usuario *usuario* ha activado el modo de pánico
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>
                </View>
    {/* -----------------------------------3------------------------------------- */}
                <View style={{paddingLeft:20,justifyContent:'center'}}>
                    <Text style={{color:'white'}}>Mira su ubicación en tiempo real:</Text>
                </View>
    {/* ------------------------------------4------------------------------------ */}
                <View style={{flex: 3}}>
                    <Content style={{paddingHorizontal:20}}>
                        <Card>
                            <CardItem style={{backgroundColor:'#00b400'}}>
                                <Body style={{paddingHorizontal:50}}>
                                    <Image style={{width:'100%',height:'100%',paddingVertical:'30%',paddingHorizontal:'70%',alignSelf:'center'}} source={require('../assets/maps.jpg')}/>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>
                </View>
    {/* -------------------------------------5----------------------------------- */}
                <View style={{flex: 2, justifyContent:'center'}}>
                <Content style={{paddingHorizontal:20}}>
                        <Card>
                            <CardItem style={{backgroundColor:'#00b400',alignContent:'center'}}>
                                <Body style={{paddingHorizontal:50}}>
                                    <Text style={{color:'white', fontSize:15,textAlign:'center'}}>
                                        10:30:15 hrs.
                                    </Text>
                                    <Text style={{color:'white', fontSize:15,textAlign:'center'}}>
                                        12/11/2020
                                    </Text>
                                    <Text style={{color:'white', fontSize:15,textAlign:'center'}}>
                                        20.4859-104.345
                                    </Text>
                                    <Text style={{color:'white', fontSize:15,textAlign:'center'}}>
                                        3
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>
                </View>
    {/* -------------------------------------6----------------------------------- */}
                <View style={{flex: 1,flexDirection:'row',justifyContent:'center',alignContent:'center'}}>
                    <Content>
                        <Card>
                            <CardItem style={{backgroundColor:'#2d7a8c'}}>
                                <Body>
                                    <Text>

                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>

                    <Content>
                        <Card>
                            <CardItem style={{backgroundColor:'#2d7a8c'}}>
                                <Body>
                                    <Text>

                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>

                    <Content>
                        <Card>
                            <CardItem style={{backgroundColor:'#2d7a8c'}}>
                                <Body>
                                    <Text>

                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>

                </View>
            </ScrollView>
    )


}

export default testScreen