import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Input } from 'react-native-elements';
import { Image, StyleSheet, View, Text, TouchableHighlight, ScrollView, Linking } from 'react-native';

const uri="https://google.com";

const loginScreen = ({navigation}) => {
	return (
	
		<LinearGradient colors={['#b4c8d5', '#b4c8d5', '#b4c8d5']} style={{flex:1}}>
			<ScrollView>
				<View style={{alignItems:'center'}}>
					<Image style={{width:350, height:350}}source={require('../assets/Logo.png')}/>
				</View>

				<View style={{alignContent:'center',paddingHorizontal:50}}>	
					<Input label='Username'/>		
					<Input label="Password" secureTextEntry={true} />
				</View>
				
				<View style={{flexDirection: 'row', justifyContent: 'center', paddingTop:20}}>
					<TouchableHighlight>
						<Text style={{color: '#4d798a'}} onPress={()=>navigation.navigate('mainScreen')}>Iniciar Sesion</Text>
					</TouchableHighlight>
				</View>

				<View style={{flexDirection:'row',justifyContent:'space-around'}}>
					<TouchableHighlight  onPress={()=>navigation.navigate('registryScreen')}>
						<Text style={{color: '#4d798a'}}>Crear cuenta</Text>
					</TouchableHighlight>
					<TouchableHighlight >
						<Text style={{color: '#4d798a'}}>Olvide mi contraseña</Text>
					</TouchableHighlight>
				</View>

				<View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:5}}>
					<TouchableHighlight onPress={()=>Linking.openURL(uri)}>
						<Text style={{color: '#4d798a'}}>Sobre B-Safe</Text>
					</TouchableHighlight>
					<TouchableHighlight>
						<Text style={{color: '#4d798a'}}>Servicio al Cliente</Text>
					</TouchableHighlight>
				</View>
			</ScrollView>
		</LinearGradient>
  	);
}


export default loginScreen;