import React from 'react';
import {StyleSheet, View, Image, Text, ScrollView, Button, TouchableHighlight} from 'react-native';

const profileScreen = () => {
  return (
    <ScrollView>
      <View style={styles.contenedor}>
        <View style={styles.botonmenu}>
          <TouchableHighlight>
            <Image style={{height: 50, width: 50}} source={require('../assets/icons/menu.png')}></Image>
          </TouchableHighlight>
        </View>

        <View style={styles.username}>
          <Text style={styles.texto}>Xime1234</Text>
        </View>

        <View style={styles.info}>
          <View style={styles.infocontainer}>
           <View style={styles.vacio}></View>
            <Image style={styles.icons} source={require('../assets/icons/10.1.png')}></Image>
            <Text style={styles.letters}> Ana Ximena </Text>
            </View>

            <View style={styles.infocontainer}>
            <View style={styles.vacio}></View>
            <Image style={styles.icons} source={require('../assets/icons/10.2.png')}></Image>
            <Text style={styles.letters}> Rodríguez Camacho </Text>
            </View>

            <View style={styles.infocontainer}>
            <View style={styles.vacio}></View>
            <Image style={styles.icons} source={require('../assets/icons/10.3.png')}></Image>
            <Text style={styles.letters}> anaximena220198@gmail.com </Text>
            </View>

            <View style={styles.infocontainer}>
            <View style={styles.vacio}></View>
            <Image style={styles.icons} source={require('../assets/icons/10.4.png')}></Image>
            <Text style={styles.letters}> 3319717154 </Text>
            </View>

            <View style={styles.infocontainer}>
            <View style={styles.vacio}></View>
            <Image style={styles.icons} source={require('../assets/icons/10.5.png')}></Image>
            <Text style={styles.letters}> ID:5124 </Text>
            </View>

            
          </View>
        <View style={styles.white}></View>
          <View style={styles.botonedit}>
         <TouchableHighlight
         style={styles.btneditar}
         >
        <View style={{flexDirection: 'row'}}>
        
         <Text style={styles.textoeditar}>Editar</Text>
         <Image style={styles.icons} source={require('../assets/icons/editar2.png')}></Image>
         </View>
         
         </TouchableHighlight>
         
          </View>

          <View style={styles.white}></View>
        
          <View style={styles.letras}>
            <Text style={styles.lette}>¿De quién soy contacto de confianza?</Text>
          </View>
          <View style={styles.usuarios}></View>
          <View style={styles.preguntas}>
            <TouchableHighlight>
              <View style={{flexDirection: 'row'}}>
                <Image style={{height:15, width:15, alignSelf:'center'}} source={require('../assets/icons/help.png')}></Image>
                <Text style={{color: '#2d7a8c'}}>  Sobre Bsafe </Text>
              </View>
            </TouchableHighlight>
            
           
            <TouchableHighlight>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: '#2d7a8c'}}>Servicio al cliente  </Text>
              <Image style={{height:15, width:15, alignSelf:'center'}} source={require('../assets/icons/info.png')}></Image>
              </View>
            </TouchableHighlight>
          </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({

contenedor: {
  backgroundColor: '#0a303f',
  flex: 1
},

texto: {
  color: "white",
  fontSize: 27,
  alignItems: 'center',
  fontWeight: 'bold'
},

botonmenu: {
  flex: 2,
  backgroundColor: '#0a303f',

},

white: {
  flex: 1,
  backgroundColor: '#0a303f',

},

username: {
  flex: 2,
  alignItems: 'center'

},

info: {
  flex: 6,
  alignItems: 'center',
  backgroundColor: '#0a303f',
  borderWidth: 0,
		borderRadius: 5,
},

botonedit: {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#0a303f'

},

letras: {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#0a303f'

},

usuarios: {
  flex: 7,
  alignItems: 'center',
  backgroundColor: '#0a303f'

},

preguntas: {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#0a303f',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingHorizontal: 15

},

icons: {
  width:30,
  height:30,
  flex: 1,
  borderWidth: 1
},

icon: {
  width:35,
  height:35
},

infocontainer:{
  flexDirection: 'row',
  borderWidth: 0,
	borderRadius: 0,
  alignItems: 'center',
  flex:3	
},

letters: {
  justifyContent: 'center',
	flexDirection: 'row',
	marginHorizontal: 40,
  color: 'white',
  alignItems: 'center',
  fontSize: 16,
  flex: 8,
  fontWeight: 'bold'

},

lette: {
  justifyContent: 'center',
	flexDirection: 'row',
	marginHorizontal: 40,
  color: 'white',
  alignItems: 'center',
  fontSize: 16,
  flex: 8,
  

},

vacio: {
  flex: 1
},

btneditar: {
  backgroundColor: '#e1eff4',
  height: 50,
  width: 120,
},

textoeditar: {
  color: '#0a303f',
  alignItems: 'center',
  fontSize: 20,
  marginHorizontal: 20
},

});

export default profileScreen;
