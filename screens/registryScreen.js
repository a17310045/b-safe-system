import React, {useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Input, Button } from 'react-native-elements';
import {Image,StyleSheet,View,Text,ScrollView} from 'react-native';

import firebase from '../database/firebase'

const registryScreen = (props) => {

	const initialState = {
		user: "",
		name: "",
		lastname: "",
		email: "",
		phone: "",
		pssword: "",
	};

	const [state, setState] = useState(initialState);

	const handleChangeText = (value, name) => {
		setState({...state, [name]: value})
	};

	const saveNewUser = async () => {
        if(state.name === "") {
            alert ("Ingrese un nombre")
        } else {
            try {
                await firebase.db.collection("users").add({
                    user: state.user,
                    name: state.name,
                    lastname: state.lastname,
                    email: state.email,
                    phone: state.phone,
                    pssword: state.pssword,
                });
                props.navigation.navigate('loginScreen')
            } catch (error) {
                console.log(error);
            }
        }
    };

	return (
		<LinearGradient colors={['#b4c8d5', '#b4c8d5', '#b4c8d5']} style={styles.linearGradient}>
			<ScrollView>
			<View style={{alignItems: "center",marginVertical: 50, backgroundColor:'#2d7a8c'}}>
				<Text style={{color: 'white', fontSize: 50,}}>REGISTRO</Text>
			</View>
			
			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/user-icon2.png')}/>
				<Input placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder={'Usuario'} style={styles.input} onChangeText={(value) => handleChangeText(value, "user")}
              value={state.user}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/name-icon.png')}/>
				<Input placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder= {'Nombre'} style={styles.input} onChangeText={(value) => handleChangeText(value, "name")}
              value={state.name}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/lastName-icon.png')}/>
				<Input placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder= {'Apellido'} style={styles.input} onChangeText={(value) => handleChangeText(value, "lastname")}
              value={state.lastname}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/mail-icon.png')}/>
				<Input keyboardType="email-address" placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder= {'E-mail'} style={styles.input} onChangeText={(value) => handleChangeText(value, "email")}
              value={state.email}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/phone-icon.png')}/>
				<Input placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder= {'Teléfono'} style={styles.input} onChangeText={(value) => handleChangeText(value, "phone")}
              value={state.phone}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/pass-icon2.png')}/>
				<Input placeholderTextColor="rgba(255, 255, 255, 0.9)" placeholder= {'Contraseña'} style={styles.input} onChangeText={(value) => handleChangeText(value, "pssword")}
              value={state.pssword}/>
			</View>

			<View style={styles.contentInput}>
				<Image style={styles.icons} source={require('../assets/icons/confirm-icon.png')}/>
				<Input placeholderTextColor="rgb(255, 255, 255)" placeholder= {'Confirmar contraseña'} style={styles.input}/>
			</View>

			<View style={styles.bottomContent}>
				<Button
                    title = 'Registrarse'
					onPress={() => saveNewUser()}
                />
			</View>

			</ScrollView>
		</LinearGradient> 
  	);
    
};

const styles = StyleSheet.create({
	nav: {
		width:350,
		height:350
	},
	contentInput: {
		justifyContent: 'center',
		flexDirection: 'row',
		marginHorizontal: 40
	},
	linearGradient: {
    	flex: 1
  	},
	
	icons:{
		width:35,
		height:35
	},
	input:{
		borderWidth: 0,
		borderRadius: 4,
		backgroundColor: '#0a303f',
		fontSize: 17,
		height: 2,
		color: '#ffffff'
	},
	bottomContent:{
		alignItems: 'center',
		justifyContent: 'center',
		
	},
	bottomText: {
		fontSize: 20,
		color: '#4d798a'
	}

});

export default registryScreen;