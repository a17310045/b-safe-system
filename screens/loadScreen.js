import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Image, StyleSheet, View} from 'react-native';



const loadScreen = ({navigation}) => {
      setTimeout( () => {
        navigation.navigate('loginScreen')
      }, 1000, this)
    return(
      <LinearGradient colors={['#b4c8d5', '#b4c8d5', '#b4c8d5']} style={styles.linearGradient}>
        <View>
        <Image
          style={styles.img}
          source={require('../assets/Logo.png')}
        />
        </View>
      </LinearGradient>
    )
  }

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  img:{
    width: 250,
    height: 250
  }
});

export default loadScreen