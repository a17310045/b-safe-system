import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Image, StyleSheet, View, Text, TouchableHighlight, ScrollView, Linking } from 'react-native';

const mainScreen = ({navigation}) => {
    return (
            <ScrollView contentContainerStyle={{flex: 1, backgroundColor:'#0a303f'}}>
                <View style={{flex: 1,flexDirection:'column',justifyContent:'center' , alignItems:'flex-start'}}>
                <Image style={{height: 50, width: 50}} source={require('../assets/icons/menu.png')}></Image>
                </View>
{/* ------------------------------------------------------------------------------------------ */}
                <View style={{flex: 1, alignItems:'center'}}>
                    {/* Falta ver el tipo de fuente */}
                    <Text style={{paddingTop: 10,color:'white'}}>Bienvenido "Usuario"</Text>
                </View>
{/* ------------------------------------------------------------------------------------------ */}
                <View style={{flex: 3}}>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-around',alignItems: 'center'}}>
                        <TouchableHighlight>
                            <Text style={{color:'white'}}>Contactos</Text>
                        </TouchableHighlight>
            {/* ----------------------------------------- */}
                        <TouchableHighlight>
                            <Text style={{color:'white'}}>Configuracion</Text>
                        </TouchableHighlight>
                    </View>
            {/* ----------------------------------------- */}
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-around',alignItems: 'center'}}>
                        <TouchableHighlight onPress={()=>navigation.navigate('profileScreen')}>
                            <Text style={{color:'white'}}>Perfil</Text>
                        </TouchableHighlight>
            {/* ----------------------------------------- */}
                        <TouchableHighlight>
                            <Text style={{color:'white'}}>Sobre B-Safe</Text>
                        </TouchableHighlight>
                    </View>
                </View>
{/* ------------------------------------------------------------------------------------------ */}
                <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                    <TouchableHighlight>
                        <Text style={{color:'white'}}>Confirmacion de boton</Text>
                    </TouchableHighlight>
            {/* ----------------------------------------- */}
                    <TouchableHighlight>
                        <Text style={{color:'white'}}>Modo de pruebas</Text>
                    </TouchableHighlight>
                </View>
{/* ------------------------------------------------------------------------------------------ */}
                <View style={{flex: 3, justifyContent:'center', alignItems:'center'}}>
                    <TouchableHighlight onPress={()=>navigation.navigate('emergencyScreen')}>
                        <Text style={{color:'white'}}>Boton de Panico</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
    )


}

export default mainScreen